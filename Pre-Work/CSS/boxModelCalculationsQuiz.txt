Sample Element

#div1 {

    height: 150px;

    width: 400px;

    margin: 20px;

    border: 1px solid red;

    padding: 10px;
}

<------------------------------------------------>

Total height: 

20px(margin-top)+1px(border-top)+10px(padding-top)+150px(content-height)+10px(padding-bottom)+1px(border-bottom)+20px(margin-bottom)= 212px

Total width: 

20(margin-left)+1(border-left)+10(padding-left)+400(content-width)+10(padding-right)+1(border-right)+20(margin-right)= 462px

Browser calculated height:

1px(border-top)+10px(padding-top)+150px(content-height)+10px(padding-bottom)+1px(border-bottom)= 172px

Browser calculated width:

1(border-left)+10(padding-left)+400(content-width)+10(padding-right)+1(border-right)= 422px